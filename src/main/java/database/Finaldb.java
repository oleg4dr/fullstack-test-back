/*
 * This file is generated by jOOQ.
 */
package database;


import database.tables.Emp;
import database.tables.Org;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Catalog;
import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.12.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Finaldb extends SchemaImpl {

    private static final long serialVersionUID = 98515602;

    /**
     * The reference instance of <code>FinalDB</code>
     */
    public static final Finaldb FINALDB = new Finaldb();

    /**
     * The table <code>FinalDB.emp</code>.
     */
    public final Emp EMP = database.tables.Emp.EMP;

    /**
     * The table <code>FinalDB.org</code>.
     */
    public final Org ORG = database.tables.Org.ORG;

    /**
     * No further instances allowed
     */
    private Finaldb() {
        super("FinalDB", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
            Emp.EMP,
            Org.ORG);
    }
}
