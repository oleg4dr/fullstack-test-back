package com.example.back;

import database.tables.pojos.Emp;
import database.tables.pojos.Org;
import model.*;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@CrossOrigin
@RequestMapping("/api")
public class DBController {
    final
    DBService service;


    public DBController(DBService service) {
        this.service = service;
    }

//    Организации

    //получить ВСЕ организации
    @GetMapping("/getOrgs")
    List<CompanyWithParentName> apiGetOrgs() {
        return service.getOrgs();
    }

    //получить организации на странице Page
    @GetMapping("/getOrgs/{page}")
    List<CompanyWithParentName> apiGetOrgsOnPage(@PathVariable String page){
        return service.getOrgsOnPage(Integer.parseInt(page));
    }

    //получить общее количество страниц
    @GetMapping("/getOrgPageCount")
    int apiGetOrgPageCount(){
        return  service.getOrgPageCount();
    }

    //удалить организацию с указанным id
    @DeleteMapping("/delOrg/{id}")
     void apiDelOrg(@PathVariable String id){
        //TODO - статусы типо ОК и тд
        //TODO - уволить работников? если надо удалить компанию в которой все еще кто-то есть
        service.delOrg(id);
    }

    //добавить организацию
    @PostMapping("/addOrg")
    CompanyWithParentName apiNewOrg(@RequestBody Org company){
        return service.newOrg(company.getOrgTitle(), company.getOrgParentId());
    }

    //Изменить организацию
    @PutMapping("/editOrg")
    CompanyWithParentName apiEditOrg(@RequestBody Org company){
        return service.editOrg(company.getOrgId(), company.getOrgTitle(), company.getOrgParentId());
    }

    //получить возможных родителей для организации с указанным id
    @GetMapping("/getValidParentOrgs/{id}")
    List<Org> apiGetValidParentOrgs(@PathVariable String id){
        return service.getValidParentOrgs(UUID.fromString(id));
    }

    //получить всех детей указанной организации
    @GetMapping("/getAllChildOrgs/{id}")
    List<Org> apiGetAllChildOrgs(@PathVariable String id){
        return service.getAllChildOrgsN(UUID.fromString(id));
    }

    //получить полное дерево организаций
    @GetMapping("/getFullOrgTree")
    List<CompanyNode> apiFullOrgTree(){
        return service.getFullOrgTree();
    }

    //найти организацию с указанным
    @GetMapping("/searchOrg/{title}")
    List<CompanyWithParentName> apiSearchOrg(@PathVariable String title) {
        return service.searchOrg(title);
    }

//    Сотрудники

    //получить всех работников
    @GetMapping("/getWorkers")
    List<WorkerWithCompany> apiGetWorkers() {
        return service.getWorkers();
    }

    //получить работников на конкретной странице
    @GetMapping("/getWorkers/{page}")
    List<WorkerWithCompany> apiGetWorkersOnPage(@PathVariable String page){
        return service.getWorkersOnPage(Integer.parseInt(page));
    }

    //найти рабоников с параметрами из формы
    @PostMapping("/searchWorkers")
    List<WorkerWithCompany> apiSearchWorker(@RequestBody WorkerSearchForm form){
        return service.searchEmp(form.getOrg(), form.getName(), form.isOr());
    }

    //получить количество страниц работников
    @GetMapping("/getWorkerPageCount")
    int apiGetWorkerPageCount(){
        return  service.getWorkerPageCount();
    }

    //удалить работника с указанным id
    @DeleteMapping("/delWorker/{id}")
    void apiDelWorker(@PathVariable String id){
        //TODO - статусы типо ОК и тд
        //TODO - уволить работников? если надо удалить компанию в которой все еще кто-то есть
        service.delWorker(id);
    }

    //добавить работника
    @PostMapping("/addWorker")
    WorkerWithCompany apiNewWorker(@RequestBody Emp worker){
        return service.newWorker(worker.getEmpName(), worker.getEmpOrgId(), worker.getEmpLeadId());
    }
    //получить возможных начальников для указанного работника
    @GetMapping("/getLeads/{id}")
    List<Emp> apiGetLeads(@PathVariable String id){
        return service.getViableLeads(UUID.fromString(id));
    }

    //изменить работника
    @PutMapping("/editWorker")
    WorkerWithCompany apiEditWorker(@RequestBody Emp worker){
        return service.editWorker(worker.getEmpId(), worker.getEmpName(), worker.getEmpOrgId(), worker.getEmpLeadId());
    }

    //получить полное дерево работников
    @GetMapping("/getFullWorkerTree")
    List<WorkerNode> apiFullWorkerTree(){
        return service.getFullWorkerTree();
    }
}
