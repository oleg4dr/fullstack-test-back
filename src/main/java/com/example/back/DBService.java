package com.example.back;

import database.tables.pojos.Emp;
import database.tables.pojos.Org;
import database.tables.records.EmpRecord;
import database.tables.records.OrgRecord;
import model.CompanyNode;
import model.CompanyWithParentName;
import model.WorkerNode;
import model.WorkerWithCompany;
import org.jooq.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import static database.tables.Emp.EMP;
import static database.tables.Org.ORG;

@Service
public class DBService {
    final
    DSLContext create;
    @Value("${confing.rowsperpage}")
    private int rowsPerPage;

    public DBService(@Qualifier("dslContext") DSLContext create) {
        this.create = create;
    }

    //Получить все
    List<CompanyWithParentName> getOrgs(){
        Table<OrgRecord> t1 = ORG.as("t1");
        Table<OrgRecord> t2 = ORG.as("t2");

        return create.select(t1.field(ORG.ORG_ID), t1.field(ORG.ORG_TITLE), t1.field(ORG.ORG_PARENT_ID), t2.field(ORG.ORG_TITLE))
                .from(t1)
                .leftOuterJoin(t2)
                .on(t1.field(ORG.ORG_PARENT_ID).eq(t2.field(ORG.ORG_ID)))
                .fetchInto(CompanyWithParentName.class);
    }

    List<WorkerWithCompany> getWorkers() {
        Table<EmpRecord> t1 = EMP.as("t1");
        Table<EmpRecord> t2 = EMP.as("t2");

        Result<Record> res = create.select()
                .from(t1)
                .leftOuterJoin(t2)
                .on(t1.field(EMP.EMP_LEAD_ID).eq(t2.field(EMP.EMP_ID)))
                .leftOuterJoin(ORG)
                .on(t1.field(EMP.EMP_ORG_ID).eq(ORG.ORG_ID))
                .fetch();
        List<WorkerWithCompany> output = new ArrayList<>();
        //TODO - Исправить - пришлось сделать так, т.к. приведение через fetchInto() не хотело работать - что-то с null
        UUID[] emp_ids = res.intoArray(t1.field(EMP.EMP_ID), UUID.class);
        String[] emp_names = res.intoArray(t1.field(EMP.EMP_NAME), String.class);
        UUID[] emp_org_ids = res.intoArray(t1.field(EMP.EMP_ORG_ID), UUID.class);
        UUID[] emp_lead_ids = res.intoArray(t1.field(EMP.EMP_LEAD_ID), UUID.class);
        String[] leads = res.intoArray(t2.field(EMP.EMP_NAME), String.class);
        String[] orgs = res.intoArray(ORG.ORG_TITLE, String.class);
        for (int i = 0; i < emp_ids.length; i++) {
            output.add(new WorkerWithCompany(emp_ids[i], emp_names[i], emp_org_ids[i], emp_lead_ids[i], orgs[i], leads[i]));
        }
        return output;
    }

    //Получить на странице
    List<CompanyWithParentName> getOrgsOnPage(int pageNumber){
        Table<OrgRecord> t1 = ORG.as("t1");
        Table<OrgRecord> t2 = ORG.as("t2");

        return create.select(t1.field(ORG.ORG_ID), t1.field(ORG.ORG_TITLE), t1.field(ORG.ORG_PARENT_ID), t2.field(ORG.ORG_TITLE))
                .from(t1)
                .leftOuterJoin(t2)
                .on(t1.field(ORG.ORG_PARENT_ID).eq(t2.field(ORG.ORG_ID)))
                .limit(rowsPerPage)
                .offset((pageNumber-1)*rowsPerPage)
                .fetchInto(CompanyWithParentName.class);
    }

    List<WorkerWithCompany> getWorkersOnPage(int pageNumber) {
        Table<EmpRecord> t1 = EMP.as("t1");
        Table<EmpRecord> t2 = EMP.as("t2");

        Result<Record> res = create.select()
                .from(t1)
                .leftOuterJoin(t2)
                .on(t1.field(EMP.EMP_LEAD_ID).eq(t2.field(EMP.EMP_ID)))
                .leftOuterJoin(ORG)
                .on(t1.field(EMP.EMP_ORG_ID).eq(ORG.ORG_ID))
                .limit(rowsPerPage)
                .offset((pageNumber-1)*rowsPerPage)
                .fetch();
        List<WorkerWithCompany> output = new ArrayList<>();
        //TODO - Исправить - пришлось сделать так, т.к. приведение через fetchInto() не хотело работать - что-то с null
        UUID[] emp_ids = res.intoArray(t1.field(EMP.EMP_ID), UUID.class);
        String[] emp_names = res.intoArray(t1.field(EMP.EMP_NAME), String.class);
        UUID[] emp_org_ids = res.intoArray(t1.field(EMP.EMP_ORG_ID), UUID.class);
        UUID[] emp_lead_ids = res.intoArray(t1.field(EMP.EMP_LEAD_ID), UUID.class);
        String[] leads = res.intoArray(t2.field(EMP.EMP_NAME), String.class);
        String[] orgs = res.intoArray(ORG.ORG_TITLE, String.class);
        for (int i = 0; i < emp_ids.length; i++) {
            output.add(new WorkerWithCompany(emp_ids[i], emp_names[i], emp_org_ids[i], emp_lead_ids[i], orgs[i], leads[i]));
        }
        return output;
    }

    //Получить количество страниц
    int getOrgPageCount(){
        return (int) Math.ceil(create.selectCount().from(ORG).fetchOne(0, double.class)/(double) rowsPerPage);
    }

    int getWorkerPageCount(){
        return (int) Math.ceil(create.selectCount().from(EMP).fetchOne(0, double.class)/(double) rowsPerPage);
    }

    //Создать
    CompanyWithParentName newOrg(String orgTitle, UUID orgParentId){
        Record record = create.insertInto(ORG, ORG.ORG_ID, ORG.ORG_TITLE, ORG.ORG_PARENT_ID)
                .values(UUID.randomUUID(), orgTitle, orgParentId)
                .returningResult(ORG.ORG_ID, ORG.ORG_TITLE, ORG.ORG_PARENT_ID)
                .fetchOne();
        //TODO - объеденить запросы
        String orgParentName = create.select(ORG.ORG_TITLE).from(ORG)
                .where(ORG.ORG_ID.eq(orgParentId))
                .fetchOneInto(String.class);
        return new CompanyWithParentName(record.into(Org.class), orgParentName);
    }

    WorkerWithCompany newWorker(String workerName, UUID workerOrgId, UUID workerLeadId){
        Record record = create.insertInto(EMP, EMP.EMP_ID, EMP.EMP_NAME, EMP.EMP_ORG_ID, EMP.EMP_LEAD_ID)
                .values(UUID.randomUUID(), workerName, workerOrgId, workerLeadId)
                .returningResult(EMP.EMP_ID, EMP.EMP_NAME, EMP.EMP_ORG_ID, EMP.EMP_LEAD_ID)
                .fetchOne();
        //TODO - объеденить запросы
        String workerOrgName = create.select(ORG.ORG_TITLE).from(ORG)
                .where(ORG.ORG_ID.eq(workerOrgId))
                .fetchOneInto(String.class);
        String workerLeadName = create.select(EMP.EMP_NAME).from(EMP)
                .where(EMP.EMP_ID.eq(workerLeadId))
                .fetchOneInto(String.class);
        return new WorkerWithCompany(record.into(Emp.class), workerOrgName, workerLeadName);
    }

    //Изменить
    CompanyWithParentName editOrg(UUID orgId, String newOrgTitle, UUID newOrgParentId){
        Record record = create.update(ORG)
                .set(ORG.ORG_TITLE, newOrgTitle)
                .set(ORG.ORG_PARENT_ID, newOrgParentId)
                .where(ORG.ORG_ID.eq(orgId))
                .returningResult(ORG.ORG_ID, ORG.ORG_TITLE, ORG.ORG_PARENT_ID)
                .fetchOne();
        //UPDATE с SELECT вообще можно объеденить?
        String orgParentName = create.select(ORG.ORG_TITLE).from(ORG)
                .where(ORG.ORG_ID.eq(newOrgParentId))
                .fetchOneInto(String.class);
        return new CompanyWithParentName(record.into(Org.class), orgParentName);
    }

    WorkerWithCompany editWorker(UUID workerId, String newWorkerName, UUID newWorkerOrgId, UUID newWorkerLeadId){
        Record record = create.update(EMP)
                .set(EMP.EMP_NAME, newWorkerName)
                .set(EMP.EMP_ORG_ID, newWorkerOrgId)
                .set(EMP.EMP_LEAD_ID, newWorkerLeadId)
                .where(EMP.EMP_ID.eq(workerId))
                .returningResult(EMP.EMP_ID, EMP.EMP_NAME, EMP.EMP_ORG_ID, EMP.EMP_LEAD_ID)
                .fetchOne();
        //эти объеденить через UNION или лучше в сет данных т.к. лид в этой же компании, но могут быть проблемы с нуллами))
        String leadName = create.select(EMP.EMP_NAME).from(EMP)
                .where(EMP.EMP_ID.eq(newWorkerLeadId))
                .fetchOneInto(String.class);
        String orgName = create.select(ORG.ORG_TITLE).from(ORG)
                .where(ORG.ORG_ID.eq(newWorkerOrgId))
                .fetchOneInto(String.class);
        return  new WorkerWithCompany(record.into(Emp.class), orgName, leadName);
    }

    //Удалить
    void delOrg(String targetId){
        //TODO - проверить есть ли работники и спросить удалять или нет
        //TODO - запретить удаление если есть дочерние компании и сругаться
        create.deleteFrom(ORG).where(ORG.ORG_ID.eq(UUID.fromString(targetId))).execute();
        System.out.println("Deleted company with id: " + targetId);
    }

    void delWorker(String workerId){
        //TODO - проверить есть ли работники и спросить удалять или нет
        //TODO - запретить удаление если есть дочерние компании и сругаться
        create.deleteFrom(EMP).where(EMP.EMP_ID.eq(UUID.fromString(workerId))).execute();
        System.out.println("Deleted worker with id: " + workerId);
    }

    //Найти
    List<CompanyWithParentName> searchOrg(String title){
        Table<OrgRecord> t1 = ORG.as("t1");
        Table<OrgRecord> t2 = ORG.as("t2");

        return create.select(t1.field(ORG.ORG_ID), t1.field(ORG.ORG_TITLE), t1.field(ORG.ORG_PARENT_ID), t2.field(ORG.ORG_TITLE))
                .from(t1)
                .leftOuterJoin(t2)
                .on(t1.field(ORG.ORG_PARENT_ID).eq(t2.field(ORG.ORG_ID)))
                .where(t1.field(ORG.ORG_TITLE).contains(title))
                .fetchInto(CompanyWithParentName.class);
    }

    List<WorkerWithCompany> searchEmp(String org, String name, Boolean isOr){
        Table<EmpRecord> t1 = EMP.as("t1");
        Table<EmpRecord> t2 = EMP.as("t2");
        Result<Record> res;
        if(name.equals(""))
            name=null;
        if(org.equals(""))
            org=null;
        if(isOr) {
            res = create.select()
                    .from(t1)
                    .leftOuterJoin(t2)
                    .on(t1.field(EMP.EMP_LEAD_ID).eq(t2.field(EMP.EMP_ID)))
                    .leftOuterJoin(ORG)
                    .on(t1.field(EMP.EMP_ORG_ID).eq(ORG.ORG_ID))
                    .where(t1.field(EMP.EMP_NAME).contains(name).or(ORG.ORG_TITLE.contains(org)))
                    .fetch();
        } else {
            res = create.select()
                    .from(t1)
                    .leftOuterJoin(t2)
                    .on(t1.field(EMP.EMP_LEAD_ID).eq(t2.field(EMP.EMP_ID)))
                    .leftOuterJoin(ORG)
                    .on(t1.field(EMP.EMP_ORG_ID).eq(ORG.ORG_ID))
                    .where(t1.field(EMP.EMP_NAME).contains(name).and(ORG.ORG_TITLE.contains(org)))
                    .fetch();
        }
        List<WorkerWithCompany> output = new ArrayList<>();
        //TODO - Исправить - пришлось сделать так, т.к. приведение через fetchInto() не хотело работать - что-то с null
        UUID[] emp_ids = res.intoArray(t1.field(EMP.EMP_ID), UUID.class);
        String[] emp_names = res.intoArray(t1.field(EMP.EMP_NAME), String.class);
        UUID[] emp_org_ids = res.intoArray(t1.field(EMP.EMP_ORG_ID), UUID.class);
        UUID[] emp_lead_ids = res.intoArray(t1.field(EMP.EMP_LEAD_ID), UUID.class);
        String[] leads = res.intoArray(t2.field(EMP.EMP_NAME), String.class);
        String[] orgs = res.intoArray(ORG.ORG_TITLE, String.class);
        for (int i = 0; i < emp_ids.length; i++) {
            output.add(new WorkerWithCompany(emp_ids[i], emp_names[i], emp_org_ids[i], emp_lead_ids[i], orgs[i], leads[i]));
        }
        return output;
    }

    List<Org> getValidParentOrgs(UUID id){
        Org target =create.selectFrom(ORG).where(ORG.ORG_ID.eq(id)).fetchOneInto(Org.class);
        ArrayList<UUID> childIds = new ArrayList<>();
        for(Org o: getAllChildOrgs(new ArrayList(), target))
            childIds.add(o.getOrgId());
        ArrayList result = new ArrayList();
        for(Org o: create.selectFrom(ORG).fetchInto(Org.class))
            if(!childIds.contains(o.getOrgId()) && !o.getOrgId().equals(id))
                result.add(o);
        return result;

    }

    List<Emp> getViableLeads(UUID target){
        return  create.selectFrom(EMP)
                .where(EMP.EMP_ORG_ID.eq(target))
                .fetchInto(Emp.class);
    }

    List<Org> getChildOrgs(UUID parentId){
        return create.selectFrom(ORG).where(ORG.ORG_PARENT_ID.eq(parentId)).fetchInto(Org.class);
    }

    //Дерево
    List<Org> getTopOrgs(){
        return create.selectFrom(ORG).where(ORG.ORG_PARENT_ID.isNull()).fetchInto(Org.class);
    }

    List<Emp> getTopWorkers(){
        return create.selectFrom(EMP).where(EMP.EMP_LEAD_ID.isNull()).fetchInto(Emp.class);
    }


    CompanyNode getOrgNodes(UUID id){
        Org target = create.selectFrom(ORG).where(ORG.ORG_ID.eq(id)).fetchOneInto(Org.class);
        return getOneFullNode(new CompanyNode(target, new ArrayList<>()));
    }

    WorkerNode getWorkerNodes(UUID id){
        Emp target = create.selectFrom(EMP).where(EMP.EMP_ID.eq(id)).fetchOneInto(Emp.class);
        return getOneFullWorkerNode(new WorkerNode(target, new ArrayList<>()));
    }

    List<CompanyNode> getFullOrgTree(){
        List<Org> orgs = getTopOrgs();
        List<CompanyNode> res = new ArrayList<>();
        for(Org o: orgs){
            res.add(getOrgNodes(o.getOrgId()));
        }
        return res;
    }

    List<WorkerNode> getFullWorkerTree(){
        List<Emp> workers = getTopWorkers();
        List<WorkerNode> res = new ArrayList<>();
        for(Emp w: workers){
            res.add(getWorkerNodes(w.getEmpId()));
        }
        return res;
    }

    CompanyNode getOneFullNode(CompanyNode node){
        List<Org> children = getChildOrgs(node.getOrgId());
        ArrayList<CompanyNode> childNodes= new ArrayList<>();
        for(Org o: children)
            childNodes.add(getOneFullNode(new CompanyNode(o, new ArrayList<>())));
        node.setChildren(childNodes);
        return node;
    }

    WorkerNode getOneFullWorkerNode(WorkerNode node){
        List<Emp> children = create.selectFrom(EMP).where(EMP.EMP_LEAD_ID.eq(node.getEmpId())).fetchInto(Emp.class);
        ArrayList<WorkerNode> childNodes= new ArrayList<>();
        for(Emp w: children)
            childNodes.add(getOneFullWorkerNode(new WorkerNode(w, new ArrayList<>())));
        node.setChildren(childNodes);
        return node;
    }

    List<Org> getAllChildOrgs(ArrayList res, Org target){
        ArrayList newRes = new ArrayList();
        List<Org> children = create.selectFrom(ORG).where(ORG.ORG_PARENT_ID.eq(target.getOrgId())).fetchInto(Org.class);
        for(Org o: children){
            res.add(o);
            newRes.add( getAllChildOrgs(res, o));
        }
        return res;
    }

    List<Org> getAllChildOrgsN( UUID target){
        Org org = create.selectFrom(ORG).where(ORG.ORG_ID.eq(target)).fetchOneInto(Org.class);
        return getAllChildOrgs(new ArrayList(), org);
    }
}
