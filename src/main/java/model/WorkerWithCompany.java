package model;

import database.tables.pojos.Emp;

import java.util.UUID;

public class WorkerWithCompany extends Emp {
    private String orgName;
    private String leadName;
    public WorkerWithCompany(UUID empId, String empName, UUID empOrgId, UUID empLeadId, String orgName) {
        super(empId, empName, empOrgId, empLeadId);
        this.orgName = orgName;
    }

    public WorkerWithCompany(UUID empId, String empName, UUID empOrgId, UUID empLeadId, String orgName, String leadName) {
        super(empId, empName, empOrgId, empLeadId);
        this.orgName = orgName;
        this.leadName = leadName;
    }

    public WorkerWithCompany(Emp value, String orgName, String leadName) {
        super(value);
        this.orgName = orgName;
        this.leadName = leadName;
    }

    @Override
    public String toString() {
        return "WorkerWithCompany{" +
                "companyName='" + orgName + '\'' +
                ", leadName='" + leadName + '\'' +
                '}';
    }

    public String getLeadName() {
        return leadName;
    }

    public void setLeadName(String leadName) {
        this.leadName = leadName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
}
