package model;

public class WorkerSearchForm {
    private String org;
    private String name;
    private boolean isOr;

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isOr() {
        return isOr;
    }

    public void setOr(boolean or) {
        isOr = or;
    }

    @Override
    public String toString() {
        return "WorkerSearchForm{" +
                "org='" + org + '\'' +
                ", lead='" + name + '\'' +
                ", isOr=" + isOr +
                '}';
    }

    public WorkerSearchForm(String org, String lead, boolean isOr) {
        this.org = org;
        this.name = lead;
        this.isOr = isOr;
    }
}
