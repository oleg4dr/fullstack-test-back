package model;

import database.tables.pojos.Emp;

import java.util.ArrayList;

public class WorkerNode extends Emp {
    ArrayList<WorkerNode> children;

    @Override
    public String toString() {
        return "WorkerNode{" +
                "children=" + children +
                '}';
    }

    public ArrayList<WorkerNode> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<WorkerNode> children) {
        this.children = children;
    }

    public WorkerNode(Emp value, ArrayList<WorkerNode> children) {
        super(value);
        this.children = children;
    }
}
