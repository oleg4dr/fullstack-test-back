package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import database.tables.pojos.Org;

import java.util.UUID;

public class CompanyWithParentName extends Org {

    private String orgParentId;
    private String parentName;

    public CompanyWithParentName(UUID orgId, String orgTitle) {
        super(orgId, orgTitle);
        this.orgParentId = "";
        this.parentName = "";
    }

    public CompanyWithParentName(UUID orgId, String orgTitle, UUID orgParentId, String parentName) {
        super(orgId, orgTitle, orgParentId);
        this.parentName = parentName;
    }

    @Override
    public String toString() {
        return "CompanyWithParentName{" +
                "parentName='" + parentName + '\'' +
                '}';
    }

    public CompanyWithParentName(Org value, String parentName) {
        super(value);
        this.parentName = parentName;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }
}
