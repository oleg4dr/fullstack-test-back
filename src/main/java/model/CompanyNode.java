package model;

import database.tables.pojos.Org;

import java.util.ArrayList;

public class CompanyNode extends Org  {
    ArrayList<CompanyNode> children;

    public CompanyNode(Org value, ArrayList<CompanyNode> children) {
        super(value);
        this.children = children;
    }

    public ArrayList<CompanyNode> getChildren() {
        return children;
    }

    public void setChildren(ArrayList<CompanyNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "ComapanyNode{" +
                "children=" + children +
                '}';
    }
}
