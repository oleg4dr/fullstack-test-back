/*
 Navicat Premium Data Transfer

 Source Server         : PostgreSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 120000
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : FinalDB

 Target Server Type    : PostgreSQL
 Target Server Version : 120000
 File Encoding         : 65001

 Date: 30/06/2020 23:55:51
*/


-- ----------------------------
-- Table structure for emp
-- ----------------------------
DROP TABLE IF EXISTS "FinalDB"."emp";
CREATE TABLE "FinalDB"."emp" (
  "emp_id" uuid NOT NULL,
  "emp_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "emp_org_id" uuid,
  "emp_lead_id" uuid
)
;

-- ----------------------------
-- Records of emp
-- ----------------------------
INSERT INTO "FinalDB"."emp" VALUES ('963fc869-30ee-4dc2-a314-561c8fe7fcc6', 'John', 'ba50cf75-aaa5-4c7d-85a2-b1bc877b8452', NULL);
INSERT INTO "FinalDB"."emp" VALUES ('29a88653-9d0c-4af8-b7a5-1381e342a5c2', 'Bob', '09a2032c-45ad-4cb2-8b07-c873ea41475c', '196937c6-7099-4154-853a-1138023db56e');
INSERT INTO "FinalDB"."emp" VALUES ('196937c6-7099-4154-853a-1138023db56e', 'Mark', NULL, NULL);

-- ----------------------------
-- Table structure for org
-- ----------------------------
DROP TABLE IF EXISTS "FinalDB"."org";
CREATE TABLE "FinalDB"."org" (
  "org_id" uuid NOT NULL,
  "org_title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "org_parent_id" uuid
)
;

-- ----------------------------
-- Records of org
-- ----------------------------
INSERT INTO "FinalDB"."org" VALUES ('af357a9d-c9b4-4b08-a569-28bcbfb74324', 'Bam''s salty cry juice ', 'ba50cf75-aaa5-4c7d-85a2-b1bc877b8452');
INSERT INTO "FinalDB"."org" VALUES ('ba50cf75-aaa5-4c7d-85a2-b1bc877b8452', 'Rachel Steaks', '155ecf4d-cd79-4d22-a575-8d9e214e88a9');
INSERT INTO "FinalDB"."org" VALUES ('155ecf4d-cd79-4d22-a575-8d9e214e88a9', 'Coffee House', NULL);
INSERT INTO "FinalDB"."org" VALUES ('09a2032c-45ad-4cb2-8b07-c873ea41475c', 'Hooters', NULL);

-- ----------------------------
-- Indexes structure for table emp
-- ----------------------------
CREATE INDEX "emp_indexes" ON "FinalDB"."emp" USING btree (
  "emp_id" "pg_catalog"."uuid_ops" ASC NULLS LAST,
  "emp_org_id" "pg_catalog"."uuid_ops" ASC NULLS LAST,
  "emp_lead_id" "pg_catalog"."uuid_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table emp
-- ----------------------------
ALTER TABLE "FinalDB"."emp" ADD CONSTRAINT "emp_pkey" PRIMARY KEY ("emp_id");

-- ----------------------------
-- Indexes structure for table org
-- ----------------------------
CREATE INDEX "org_indexes" ON "FinalDB"."org" USING btree (
  "org_id" "pg_catalog"."uuid_ops" ASC NULLS LAST,
  "org_parent_id" "pg_catalog"."uuid_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table org
-- ----------------------------
ALTER TABLE "FinalDB"."org" ADD CONSTRAINT "org_pkey" PRIMARY KEY ("org_id");

-- ----------------------------
-- Foreign Keys structure for table emp
-- ----------------------------
ALTER TABLE "FinalDB"."emp" ADD CONSTRAINT "emp_lead_ref" FOREIGN KEY ("emp_lead_id") REFERENCES "FinalDB"."emp" ("emp_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "FinalDB"."emp" ADD CONSTRAINT "emp_org_ref" FOREIGN KEY ("emp_org_id") REFERENCES "FinalDB"."org" ("org_id") ON DELETE SET NULL ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table org
-- ----------------------------
ALTER TABLE "FinalDB"."org" ADD CONSTRAINT "org_parent_ref" FOREIGN KEY ("org_parent_id") REFERENCES "FinalDB"."org" ("org_id") ON DELETE NO ACTION ON UPDATE NO ACTION;
