**Серверная часть fullstack задания**

**Инструкция по запуску:**
1.  Импортировать БД - дамп файл `FinalDB.sql` в папке `src/main/resources`
2.  Изменить под свою систему настройки jooq в файле `src/main/resources/application.properties`
3.  Запустить 

Клиентская часть расположена [здесь](https://gitlab.com/oleg4dr/fullstack-test-front)

**API**
1. Получить все \
` GET /api/getOrgs` - организации\
` GET /api/getWorkers` - сотрудников

2. Получить общее количество страниц\
` GET /api/getOrgPageCount` - организации\
` GET /api/getWorkerPageCount` - сотрудников

3. получить на конкретной странице\
` GET /api/getOrgs/{page}` - организации\
` GET /api/getWorkers/{page}` - сотрудников

4. удалить с указанным id\
`DELETE /api/delOrg/{id}` - организацию\
` DELETE /api/delWorker/{id}` - сотрудника

5. добавить\
`POST /api/addOrg` - организацию\
тело запроса:\
`orgTitle: {имя организации}`\
`orgParentId: {id организации родителя}`\
`POST /api/addWorker` - сотрудника\
тело запроса:\
`empName: {имя работника}`\
`empOrgId: {id организации}`\
`empLeadId: {id руководителя}`\

6. Изменить\
`POST /api/editOrg` - организацию\
тело запроса:\
`orgId: {id организации}`\
`orgTitle: {имя организации}`\
`orgParentId: {id организации родителя}`\
`POST /api/editWorker` - сотрудника\
тело запроса:\
`empId: {id организации}`\
`empName: {имя работника}`\
`empOrgId: {id организации}`\
`empLeadId: {id руководителя}`\

7. Найти\
`GET /api/searchOrg/{title}` - организацию\
`GET /api/getAllChildOrgs/{id}` - все дочерние организации (независимо от глубины) \
`GET /api/getValidParentOrgs/{id}` - валидных родителей для орагнизации\
` GET /api/getLeads/{id}` - возможных начальников для работника\
` GET /api/searchWorkers/` - сотрудника с указанными параметрами\
тело запроса:\
`org: {имя организации}`\
`name: {имя работника}`\
`isOr: {ИЛИ/И}`\

8. Дерево\
`GET /api/getFullOrgTree` - организаций\
`GET /api/getFullWorkerTree` - сотрудников
